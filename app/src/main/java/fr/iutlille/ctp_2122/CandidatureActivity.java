package fr.iutlille.ctp_2122;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import fr.iutlille.ctp_2122.databinding.ActivityCandidatureBinding;

public class CandidatureActivity extends AppCompatActivity {

    ActivityCandidatureBinding ui;
    Candidature candidature;


    /* TODO Q4 (Décommenter) */
    /*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ui = ActivityCandidatureBinding.inflate(this.getLayoutInflater());
        setContentView(ui.getRoot());
        Intent intent = getIntent();
        ParcoursBUT parcoursBUT = (ParcoursBUT) getApplication();
        int position = intent.getIntExtra("position", -1) ;
        if (position != -1) {
            candidature = parcoursBUT.getCandidatures().get(position);
            ui.edtNom.setText(candidature.getNom());
            ui.edtLimite.setDate(candidature.getLimite().getTime());
            switch (candidature.getEtat()) {
                case NOPE:
                    ui.edtEtat.check(R.id.rbNope);
                    break;
                case TODO:
                    ui.edtEtat.check(R.id.rbTodo);
                    break;
                case DONE:
                    ui.edtEtat.check(R.id.rbDone);
                    break;
                case LATE:
                    ui.edtEtat.check(R.id.rbLate);
                    break;
            }
        }
        ui.edtLimite.setOnDateChangeListener((view, year, month, day) -> {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            ui.edtLimite.setDate(calendar.getTimeInMillis());
        });
    }
    public void doValider(View view) {
        candidature.setNom(String.valueOf(ui.edtNom.getText()));
        long limite = ui.edtLimite.getDate();
        candidature.setLimite(limite);
        int checked = ui.edtEtat.getCheckedRadioButtonId();
        if (checked == R.id.rbNope) {
            candidature.setEtat(Candidature.Etat.NOPE);
        } else if (checked == R.id.rbTodo) {
            candidature.setEtat(Candidature.Etat.TODO);
        } else  if (checked ==  R.id.rbDone) {
            candidature.setEtat(Candidature.Etat.DONE);
        } else if (checked == R.id.rbLate) {
            candidature.setEtat(Candidature.Etat.LATE);
        }
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
    */
}