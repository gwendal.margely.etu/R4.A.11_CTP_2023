package fr.iutlille.ctp_2122;  // ligne à modifier


import androidx.appcompat.app.AppCompatActivity;
import java.util.List;
import android.os.Bundle;
import android.widget.TextView;

import fr.iutlille.ctp_2122.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private final int active = 0;
    private ActivityMainBinding ui;
    private List<Candidature> candidatures;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ui = ActivityMainBinding.inflate(this.getLayoutInflater());
        setContentView(ui.getRoot());
        ParcoursBUT application = (ParcoursBUT) getApplication();
        candidatures = application.getCandidatures();
        this.showStats();
        /* TODO Q3a: Menu contextuel */
        /* TODO Q3: RecyclerView */
    }

    /* TODO Q1: statistiques */
    public void showStats() {
        TextView textViewProspects = (TextView) findViewById(R.id.Title);
        textViewProspects.setText(candidatures.size() + " prospects");
        TextView textViewNope = (TextView) findViewById(R.id.nbNope);
        TextView textViewTodo = (TextView) findViewById(R.id.nbTodo);
        TextView textViewDone = (TextView) findViewById(R.id.nbDone);
        TextView textViewLate = (TextView) findViewById(R.id.nbLate);
        int nNope = 0;
        int nTodo = 0;
        int nDone = 0;
        int nLate = 0;
        for(Candidature i : candidatures) {
            switch(i.getEtat()) {
                case NOPE:
                    nNope++;
                case TODO:
                    nTodo++;
                case DONE:
                    nDone++;
                case LATE:
                    nLate++;
            }
        }
        textViewNope.setText("" + nNope);
        textViewTodo.setText("" + nTodo);
        textViewDone.setText("" + nDone);
        textViewLate.setText("" + nLate);
    }
    /* TODO Q2: A propos */

    /* TODO Q3a: Menu contextuel */

    /* TODO Q3b: Tri de liste */

}